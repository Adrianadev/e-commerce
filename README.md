# E-commerce
Projeto de loja vitual Mirror Fashion
Baseado na apostila "[Desenvolvimento web com html, css e javascript](https://gitlab.com/estudos6314604/e-commerce/-/blob/main/textos/Desenvolvimento-web-html-css-javascript-php.pdf?ref_type=heads)" do [Marco Bruno](https://github.com/marcobrunodev).

A Apostila introduz ao PHP, Bootstrap, jQuery, Saas, Banco de dado SQL e conceitos como responsividade, html semântico, progressive enhancement e mobile-first.
